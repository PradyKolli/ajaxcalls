# W08 Ajax Example

A simple Ajax file example.

## How to use

Open index.html in FireFox.

See jQuery Quick API Reference at https://oscarotero.com/jquery/

## Cross origin requests (CORS)

In Chrome, you will likely get an error (depending on your settings):

> Failed to load https://developer.mozilla.org/: Redirect from 'https://developer.mozilla.org/' to 'https://developer.mozilla.org/en-US/' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. Origin 'null' is therefore not allowed access.

Open with Firefox instead.

## To use in chrome

>Install <a href="https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb">Web Server for Chrome</a>
  >Click "Launch App"
  >Click "Choose Folder" and select this folder on your machine
  >Set the web server to "started"
  >Find or set the Port in the attributes below
  >Open Chrome to "localhost:[port]"  where [port] is the port specified.